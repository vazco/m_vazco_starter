Package.describe({
    summary: "Chat (facebook style) for Universe CMS"
});

Package.on_use(function (api) {
    api.use([
        'universe-core',
        'universe-core-plugin',
        'universe-core-classes'
    ]);

    api.imply([
        'universe-core'
    ]);

    api.use(['templating'], 'client');

    api.add_files([
        'plugin.js',
        'Chatrooms.js',
        'Messages.js'
    ]);

    api.add_files([
        'chatWindow.html'
    ], 'client');

    api.add_files([
        'publications.js'
    ], 'server');

    api.export('UniChat');
});