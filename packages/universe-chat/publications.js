UniChat.addPublication('UniChatChatrooms', function () {
    return UniChat.Chatrooms.find();
});

UniChat.addPublication('UniChatMessages', function (chatroom) {
    if (chatroom) {
        var messages = Uni.findRelatedTo(
            chatroom,
            'chatroom',
            UniChat.Messages
        );
        if (messages) {
            return messages;
        }
    }
    return UniChat.Messages.find();
});