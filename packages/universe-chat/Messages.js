// !!! Check Chatrooms.js for instructions !!!

Uni.Class.Message = Uni.Class.Object.extend({
    _authorKey: 'author',
    _chatroomKey: 'chatroom',
    _readedKey: 'readed',
    getAuthor: function () {
        return this.findFirstRelated(this._authorKey);
    },
    getChatroom: function () {
        return this.findFirstRelated(this._chatroomKey);
    },
    isReadedBy: function (user) {
        return this.hasRelation(this._readedKey, user);
    },
    markAsReaded: function (user) {
        return this.addRelations(this._readedKey, user);
    },
    getAuthorFullName: function () {
        var author = this.getAuthor();
        if (author) {
            return author.getFullName();
        }
    }
});

UniChat.addCollection('Messages', {
    constructor: Uni.Class.Message
});