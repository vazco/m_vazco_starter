// Let make some new class first.

// We take Uni.Group as a base. This class provides
// basic functionality for creating groups so it
// will be handy for chatroom. Check
// universe-core/Doc/Group.js for more.
Uni.Class.Chatroom = Uni.Class.Group.extend({

    // Instead of hardcoding names of relation
    // everywhere, I put them into variables.
    _messageKey: 'message',


    // As you can guess this will send message.
    sendMessage: function (message, author) {
        var uniId,
            messageObj;
        if (message && author) {

            // Let's create plain js object with our message.
            messageObj = {
                message: message
            };

            // appendRelations adds relation to plain js object.
            // We create relation to author document.
            // As a relation name we use use _authorKey from
            // message prototype.
            Uni.appendRelations(
                messageObj,
                author,
                Uni.Class.Message.prototype._authorKey
            );

            // insertWithRelations is just shortcut that use appendRelations and then
            // insert that into collection. It also returns UniId which we use later.
            // Here we insert our message while also adding relation to chatroom doc.
            // Last parameter is collection where you want to put document.
            uniId = Uni.insertWithRelations(
                messageObj,
                this,
                Uni.Class.Message.prototype._chatroomKey,
                UniChat.Messages
            );

            // here we use uniId we get from insertWithRelations to add relation
            // to this document (chatroom instance). That way we have relation
            // to message from chatroom and relation to chatroom from message.
            this.addRelations(uniId, this._messageKey);
        }
    },
    getMessages: function () {
        // findRelatedToMe takes relation name and collection and returns
        // cursor with all document related to this instance.

        // Using this we can get all messages that have 'chatroom' relation
        // to this chatrooom.
        return this.findRelatedToMe(
            Uni.Class.Message.prototype._chatroomKey,
            UniChat.Messages
        );
    },
    markAllReaded: function (user) {
        // How about updating or removing related docs?
        // Simplest way to do this is simply get all related
        // messages, loop over them and use markAsReaded method
        // from message prototype. But that is not very fast.

        // We use updateRelatedOfType method that update all
        // related documents. We can generate mongo modifier
        // using private method Uni._getAddRelationMod.

        // I skip details as it is not usual use case and
        // solution is kind of ugly.
        var mod = Uni._getAddRelationMod(
            Uni._toUniIds(user),
            Uni.Class.Message.prototype._readedKey
        );
        return this.updateAllRelatedOfType(
            Uni.Class.Message.prototype._chatroomKey,
            mod
        )
    }
});

// Default member relation key for Uni.Group is member.
// this method allows me to change that.

// So now members are not inside 'member' relation but
// inside 'chatroomMember'.
Uni.Class.Chatroom.prototype.setMemberKey('chatroomMember');


// I put methods inside constructor to get something like
// static methods.

// In here i take some users document and
// use insertWithRelations to create new
// chatroom with members inside.
Uni.Class.Chatroom.addChatroom = function (users) {
    Uni.insertWithRelations(
        {},
        users,
        Uni.Class.Chatroom.prototype._memberKey,
        UniChat.Chatrooms
    );
};

// So far nothing really happened since we did not even
// create our collection.

// Lets do this now:
// We take our plugin instance (UniChat) that we create before and
// use addCollection method to register that collection to be created.
// Again, nothing really happened now. We just tell Universe Plugin that
// in project we would like to have that collection.

// First parameter is the name of the collection. Keep in mind that in
// project this will be 'plugify' to UniChatChatrooms. You can also access
// this using UniChat.Chatroom reference.

// Check Uni.createCollection for more info.
UniChat.addCollection('Chatrooms', {
    constructor: Uni.Class.Chatroom
});