// Before everything else we create plugin instance like this:
UniChat = new Uni.Plugin('UniChat', {
    path: '/muu'
});

console.log(UniChat);
// This is basic hook that will fire after all other initializations.
UniChat.addAfterHook('UniChatCleanUp', function () {
    if (Meteor.isServer) {
        Meteor.startup(function () {
            // remove all spectator from all chatrooms on server start
            UniChat.Messages
                .update({}, {$unset: {spectators: ''}}, {multi: true});
        });
    }
});