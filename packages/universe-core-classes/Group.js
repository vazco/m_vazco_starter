'use strict';

Uni.Class.Group = Uni.Class.Doc.extend({
    _memberKey: 'members',
    addMember: function(doc) {
        return doc.addMutualRelation(this._memberKey, this);
    },
    removeMember: function(doc) {
        return doc.removeMutualRelation(this._memberKey, this);
    },
    isMember: function (doc) {
        if (doc instanceof Uni.Class.Doc) {
            return doc.hasRelation(this._memberKey, this);
        } else if (doc instanceof Uni.Class.UniId) {
            return this.hasRelation(this._memberKey, doc);
        }
    },
    getMembersRelations: function() {
        this.getRelations(this._memberKey);
    },
    getMembers: function() {
        return Uni.findDocs(this.getMembersRelations());
    },
    getMembersOfType: function(type) {
        return Uni.findDocsOfType(this.getMembersRelations(), type);
    },
    setMemberKey: function(newMemberKey){
        this._memberKey = newMemberKey;
    }
});