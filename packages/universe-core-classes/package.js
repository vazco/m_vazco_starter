Package.describe({
    summary: 'Universe CMS Classes package'
});

Package.on_use(function (api) {
    api.use([
        'universe-core',
        'underscore',
        'accounts-base',
        'simple-schema'
    ]);

    api.imply([
        'universe-core'
    ]);

    api.add_files([
        'Doc.js',
        'Object.js',
        'User.js',
        'Group.js'
    ]);
});