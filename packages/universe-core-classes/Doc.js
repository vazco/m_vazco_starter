'use strict';

Uni.Class.Doc.schema = new SimpleSchema({
    create_ts: {
        type: Date,
        autoValue: function() {
            if (this.isInsert || this.isUpsert) {
                return new Date();
            }
        }
    },
    edit_ts: {
        type: Date,
        optional: true,
        autoValue: function() {
            if (this.isUpdate) {
                return new Date();
            }
        }
    },
    relations: {
        type: Object,
        optional: true,
        blackbox: true
    }
});