'use strict';

Uni.Class.User = Uni.Class.Doc.extend({
    getFullName: function() {
        var result = '';
        if (this.profile) {
            result += _.isString(this.profile.firstName) ? this.profile.firstName : '';
            result += ' ';
            result += _.isString(this.profile.lastName) ? this.profile.lastName : '';
        }
        return result.trim();
    }
});

// Meteor.users is now UniUsers
(Meteor.isClient ? window : global).UniUsers = Meteor.users;
// And we remember that for UniId
UniUsers._referenceName = 'UniUsers';

// Hacky way to transform Meteor.users collection.
// TODO: find a better way.
// One way to do this is to wrap collection and override find and findOne methods
UniUsers._transform = function (doc) {
    return new Uni.Class.User(doc, UniUsers, Uni.cols.Users);
};

// Making user Col
Uni.cols.Users = new Uni.Col(UniUsers);


// Schema from https://github.com/aldeed/meteor-collection2.
// TODO: adjust this to our needs.
var countrySchema = new SimpleSchema({
    name: {
        type: String
    },
    code: {
        type: String,
        regEx: /^[A-Z]{2}$/
    }
});

var profileSchema = new SimpleSchema({
    firstName: {
        type: String,
        regEx: /^[a-zA-Z-]{2,25}$/,
        optional: true
    },
    lastName: {
        type: String,
        regEx: /^[a-zA-Z]{2,25}$/,
        optional: true
    },
    birthday: {
        type: Date,
        optional: true
    },
    gender: {
        type: String,
        allowedValues: ['Male', 'Female'],
        optional: true
    },
    organization: {
        type: String,
        regEx: /^[a-z0-9A-z .]{3,30}$/,
        optional: true
    },
    website: {
        type: String,
        regEx: SimpleSchema.RegEx.Url,
        optional: true
    },
    bio: {
        type: String,
        optional: true
    },
    country: {
        type: countrySchema,
        optional: true
    }
});

var userSchema = new SimpleSchema([
    Uni.Class.Doc.schema,
    {
        _id: {
            type: String,
            regEx: SimpleSchema.RegEx.Id
        },
        username: {
            type: String,
            regEx: /^[a-z0-9A-Z_]{3,15}$/,
            optional: true
        },
        emails: {
            type: [Object]
        },
        'emails.$.address': {
            type: String,
            regEx: SimpleSchema.RegEx.Email
        },
        'emails.$.verified': {
            type: Boolean
        },
        createdAt: {
            type: Date
        },
        profile: {
            type: profileSchema,
            optional: true
        },
        services: {
            type: Object,
            optional: true,
            blackbox: true
        }
    }
]);

// Due to problems with collection2 this is temporary disabled.
//UniUsers.attachSchema(userSchema);
