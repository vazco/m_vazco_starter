UniContent = new Uni.Plugin('UniContent',{
    path: 'c'
});

UniContent._prefix = 'Uni';
UniContent._suffix = 'Col';

var allowFunc = function(){
    return true;
};

UniContent.getCollection = function (type) {
    return Uni.getCollection(this._getCollectionName(type));
};

UniContent._getCollectionName = function (name) {
    return this._getPluginName(name) + this._suffix;
};

UniContent.getPlugin = function (name) {
    return Uni.global[this._getPluginName(name)];
};

UniContent._getPluginName = function (name) {
    return this._prefix + Uni._capitalLetter(name)
};

Uni.Plugin.prototype.hasFeature = function (name) {
    return this._content &&
        this._content.features &&
        this._content.features[name];
};


UniContent.create = function (options) {
    var plugin,
        globalName,
        name;
    options = options || {};

    if (_.isString(options.name)) {
        name = Uni._capitalLetter(options.name);

        globalName = this._getPluginName(name);
        plugin = new Uni.Plugin(globalName);

        plugin._content = options;

        plugin.addCollection(this._getCollectionName(name), {
            constructor: this.generateSchema(options),
            onCreated: function (collection) {
                // temporary
                collection.allow({
                    insert: allowFunc,
                    update: allowFunc,
                    remove: allowFunc
                });
            }
        });

        if(Meteor.isClient){
            _(options.templates).each(function(replace, template){
                plugin.setTemplate(template, replace);
            });
        }

        return plugin;
    } else {
        throw new Error('Content must have name');
    }
};

UniContent._fieldsDef = {
    text: {
        type: String,
        optional: true
    },
    tags: {
        type: [String],
        optional: true
    }
};

UniContent.generateSchema = function (options) {
    var className = this._getCollectionName(options.name),
        constructor = Uni.Class[className] = Uni.Class.Object.extend({
            type: options.name.toLowerCase()
        });

    constructor.schema = new SimpleSchema([
        Uni.Class.Object.schema,
        this._fieldsDef
    ]);
    return constructor;
};