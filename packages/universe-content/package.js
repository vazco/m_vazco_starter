Package.describe({
    summary: "content package"
});

Package.on_use(function (api) {
    api.use([
        'universe-core',
        'universe-core-plugin',
        'universe-core-classes',
        'simple-schema'
    ]);

    api.imply([
        'universe-core'
    ]);

    api.use([
        'templating',
    ], 'client');

    api.add_files([
        'Content.js'
    ]);

    api.add_files([
        'generics.html',
        'defaults.html',
        'client.js'
    ], 'client');

    api.add_files([
        'server.js'
    ], 'server');

    api.export('UniContent');
});