UniContent.addPublications({
    UniContentList: function (type, category) {
        return UniContent.getCollection(type).find({category: category});
    },
    UniContent: function (id, type) {
        return UniContent.getCollection(type).find(id);
    }
})