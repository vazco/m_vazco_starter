var getContentType = function () {
    return Router.current().params.type;
};

var getPluginTemplate = function(template){
    var type = getContentType(),
        plugin = UniContent.getPlugin(type);

    if (plugin) {
        return plugin.getTemplate(template + 'Default');
    }
};

var getTemplateHandler = function (template) {
    return getPluginTemplate(template);
};

UI.registerHelper('UniContentGetType', function () {
    return getContentType();
});

UI.registerHelper('UniContentGetPlugin', function () {
    return UniContent.getPlugin(getContentType())
});

UniContent.addRoutes({
    UniContentList: {
        path: ':type/:category?',
        onBeforeAction: function () {
            Meteor.subscribe('UniContentList', this.params.type, this.params.category);
        }
    },
    UniContentFull: {
        path: ':type/s/:_id/:seo/',
        onBeforeAction: function () {
            Meteor.subscribe('UniContent', this.params._id, this.params.type);
        }
    }
});


UniContent.addHelpers(
    'UniContentList',
    {
        getTemplate: function () {
            return getTemplateHandler('UniContentList');
        },
        getContext: function () {
            var type = getContentType();
            return {
                contents: UniContent.getCollection(type).find()
            };
        }
    }
);

UniContent.addHelpers(
    'UniContentCategories',
    {
        getTemplate: function () {
            return getTemplateHandler('UniContentCategories');
        }
    }
);

UniContent.addHelpers(
    'UniContentListEntry',
    {
        getTemplate: function () {
            return getTemplateHandler('UniContentListEntry');
        }
    }
);

UniContent.addHelpers(
    'UniContentFull',
    {
        getTemplate: function () {
            return getTemplateHandler('UniContentFull');
        },
        getContext: function () {
            var type = getContentType(),
                id = Router.current().params._id;
            return  UniContent.getCollection(type).findOne(id);
        }
    }
);