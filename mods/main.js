Router.configure({
    layoutTemplate: 'layout',
    notFoundTemplate: 'NotFoundPage',
    loadingTemplate: 'LoadingPage',
    templateNameConverter: 'camelCase',
    routeControllerNameConverter: 'upperCamelCase'
});


Router.map(function () {
    this.route('index', {
        path: '/'
    });
});

UniNews = UniContent.create({
    name: 'News',
    fields: {
    },
    features: {
        tags: true,
        videos: true
    },
    categories: [
        'first',
        'second',
        'third'
    ],
    templates: {
        UniContentFullDefault: 'UniNewsFull'
    }
});

UniBlog = UniContent.create({
    name: 'Blog',
    fields: {
    },
    features: {
        tags: true,
        videos: true
    }
});

Uni.init();




SimpleSchema.debug = true;


if (Meteor.isServer) {
    Meteor.publish(null, function () {
        return [
            UniUsers.find()
        ];
    });
}

var giveMeTrue = function () {
    return true;
};

if (Meteor.isClient) {
    Meteor.subscribe('UniChatChatrooms');
    Meteor.subscribe('UniChatMessages');
}

UniUsers.allow({
    insert: giveMeTrue,
    update: giveMeTrue,
    remove: giveMeTrue
});

UniChat.Chatrooms.allow({
    insert: giveMeTrue,
    update: giveMeTrue,
    remove: giveMeTrue
});

UniChat.Messages.allow({
    insert: giveMeTrue,
    update: giveMeTrue,
    remove: giveMeTrue
});